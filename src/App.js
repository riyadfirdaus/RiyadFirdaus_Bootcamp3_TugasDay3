import React, { useState } from "react";
import "./App.css";
import Header from "./components/Header";
import UserWindow from "./components/UserWindow";
import {
  Modal,
  TextField,
  Box,
  Button,
  AppBar,
  Toolbar,
  Typography,
} from "@mui/material/";

function App() {
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    boxShadow: 24,
    p: 4,
    borderRadius: 5,
  };

  const [userData, setUserData] = useState({});
  const [modalTrigger, setModalTrigger] = useState(false);
  const [user, setUser] = useState([]);

  function addUser(x) {
    setUser((user) => [...user, x]);
    setUserData({});
  }

  function handleModal() {
    setModalTrigger(true);
  }
  function handleClose() {
    setModalTrigger(false);
  }

  return (
    <div className="App">
      <div className="container">
        <AppBar position="static">
          <Toolbar sx={{ justifyContent: "space-between" }}>
            <Typography variant="h6" component="div" sx={{}}>
              Tugas Day 3
            </Typography>
            <Button onClick={handleModal} color="inherit">
              Add User
            </Button>
          </Toolbar>
        </AppBar>
        <UserWindow user={user} />
      </div>
      <Modal
        open={modalTrigger}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Box>
            <TextField
              label="Name"
              fullWidth
              variant="outlined"
              onChange={(e) =>
                setUserData({ ...userData, name: e.target.value })
              }
            />
            <TextField
              sx={{ mt: 1 }}
              label="Address"
              fullWidth
              variant="outlined"
              onChange={(e) =>
                setUserData({ ...userData, address: e.target.value })
              }
            />
            <TextField
              sx={{ mt: 1 }}
              label="Hobby"
              fullWidth
              variant="outlined"
              onChange={(e) =>
                setUserData({ ...userData, hobby: e.target.value })
              }
            />
          </Box>

          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <Button
              sx={{ mt: 1, mr: 1 }}
              onClick={() => {
                addUser(userData);
                handleClose();
              }}
              variant="contained"
              color="success"
            >
              Save
            </Button>
            <Button
              sx={{ mt: 1 }}
              onClick={handleClose}
              variant="contained"
              color="error"
            >
              Close
            </Button>
          </Box>
        </Box>
      </Modal>
    </div>
  );
}

export default App;
