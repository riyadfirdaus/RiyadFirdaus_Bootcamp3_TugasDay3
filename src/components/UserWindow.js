import { Box } from "@mui/material";
import React from "react";
import UserItem from "./UserItem";
import cssStyle from "./UserWindow.module.css";

function UserWindow(props) {
  const style = {
    display: "flex",
    flexDirection: "column",
  };

  console.log(props.user);
  if (props.user.length == 0) {
    return <div className={cssStyle.empty}>0 User</div>;
  } else
    return (
      <Box className={cssStyle.window} sx={style}>
        {props.user.map(({ name, address, hobby }, index) => {
          return (
            <UserItem key={index} name={name} address={address} hobby={hobby} />
          );
        })}
      </Box>
    );
}

export default UserWindow;
